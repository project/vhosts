<?php
?>

# Drupal <?php print "$version\n"; ?>
<VirtualHost *>
  ServerName drupal-<?php print $version; ?>.<?php print "$base\n"; ?>
<?php if ($aliases) { ?>  ServerAlias <?php print "$aliases\n" ?><?php } ?>
  ServerAdmin <?php print "$admin\n" ?>

  DocumentRoot <?php print "$document_root\n" ?>
  <Directory <?php print $document_root; ?>>
    AllowOverride FileInfo AuthConfig Limit Indexes Options
    Options MultiViews Indexes SymLinksIfOwnerMatch IncludesNoExec
    Order allow,deny
    Allow from all
  </Directory>
</VirtualHost>
